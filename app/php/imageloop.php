<?php
$number = 0;
foreach ($dirPaths as $path) {
  $images = glob($path . "/*");

  ?>
  <div data-number='<?php echo $number ?>' class='loaded img-section <?php echo substr($path, strpos($path, "/") + 1)?> <?php echo $number ?>'>

    <?php
      foreach($images as $image) {?>
            <div class='images' style='background:url("<?php echo $image ?>"); background-size:cover'></div>
    <?php } ?>

  </div>

<?php } ?>
