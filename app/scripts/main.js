/*jslint browser: true*/
/*global $, alert*/
/*jshint -W030 */

(function () {
  'use strict';
  var $homeBtn = $('#home-button');
  var $landing = $('#landing');
  var $work = $('#work');
  var $about = $('#about');
  var $slider = $('#slider');
  var $sliderSection = $('#slider > section');
  var $leftNavA = $('#left-nav a');

  function activeSection(thisObj) {

    if (thisObj.data('select') === 'work') {
      $work.css('display', 'block');
      setTimeout(function () {
        $work.velocity({opacity: 1}, 500);
      }, 1000);
    } else if (thisObj.data('select') === 'about') {
      $about.css('display', 'block');
      setTimeout(function () {
        $about.velocity({opacity: 1}, 500);
      }, 1000);
    }
  }


  function openSlider() {
    $slider.velocity({'left': 0}, {
      duration: 1000,
      easing: 'easeOutQuad'
    });
    setTimeout(function() {
      $homeBtn.velocity({'left': 0}, {
        duration: 1000,
        easing: 'easeOutQuad',
      });
    }, 250);


    $landing.velocity({'left': 200}, {
      duration: 1000,
      easing: 'easeOutQuad'
    });
  }

  function closeSlider() {
    $slider.add($homeBtn).velocity({'left': '-100%'}, {
      duration: 1000,
      easing: 'easeOutQuad'
    });
    $landing.velocity({'left': 0}, {
      duration: 1000,
      easing: 'easeOutQuad'
    });

    setTimeout(function () {
      $sliderSection.css({'opacity': '0', 'display': 'none'});
    }, 1000);

  }

  $leftNavA.on('click', function () {
    activeSection($(this));
    openSlider();
  });

  $homeBtn.on('click', function () {//back to home
    closeSlider();
  });

}());
