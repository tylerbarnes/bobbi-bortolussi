/*jslint browser: true*/
/*global $, console*/
/*jshint -W030 */

(function () {
  'use strict';

  $.getJSON('php/paths.php', function(jsondata) {//get list of folders in photos folder and create <li>'s with folder names
    var number = 0; //add data number to each <li>
    $.each(jsondata, function(index, value) {
      var valueTrunc = value.split("/").pop();//remove part of url before /
      $('#work ul').append("<li data-number=" + number + " class='" + valueTrunc + " " + index + "'>" + valueTrunc+ "</li>");
      number = number + 1;
    });

    $('#work li').on('click', function () {//switch active li on click and change to the appropriate photo section

      for (var i = 0; i < jsondata.length; i++) {
        var json = jsondata[i].split("/").pop();
        if ($(this).hasClass( json )) {
          $(this).parent().children().removeClass('selected');
          $(this).addClass('selected');
          $('div.img-section').hide();
          var $jsonClass = $('.' + json);
          $jsonClass.show();
          if (!$(this).hasClass('loaded')) {
            $(this).addClass('loaded');
            var dataStyle = [];
            $jsonClass.children().each(function () {//get array of all atrributes within data-style of selected section children
              var styleAttr = $(this).attr('data-style');
              dataStyle.push(styleAttr);
            });
            var childCount = $jsonClass.children().length;
            console.log(childCount);
            var x;
            for (x = 0; x < childCount; x++) {
              $jsonClass.children().removeAttr('data-style');
              $jsonClass.children().eq(x).attr('style', dataStyle[x]);
            }
          }
          break;
        }
      }
    });
    $('#work li:first-of-type').click();
  });

  var $fullscreenPhoto = $('#fullscreen-photo');
  var $photo = $('#photo');
  var $homeBtn = $('#home-button');
  var fadeSpeed = 200;
  var $next = $('#next-button');
  var $previous = $('#previous-button');
  var $exit = $('#exit-button');

  var imageArray = [];
  var currentImage;
  var siblings;
  var count;

  $('.images').on('click', function() {//open fullscreen photo
    imageArray = [];
    siblings = [];
    count = [];
    var $this = $(this);
    currentImage = $this.index();
    siblings = $this.parent().children();
    count = siblings.length;

    siblings.each(function () {
      var background = $(this).css('background-image');
      imageArray.push(background);
    });

    $photo.css('background-image', imageArray[currentImage]);
    $fullscreenPhoto.velocity('fadeIn', {duration: fadeSpeed}).addClass('open');
    $homeBtn.velocity('fadeOut', {duration: fadeSpeed}).addClass('open');
  });
  function next() {
    if (currentImage < (count - 1)) {
      currentImage++;
      $photo.css('background-image', imageArray[currentImage]);
    }
  }

  function previous() {
    if (currentImage > 0) {
      currentImage--;
      $photo.css('background-image', imageArray[currentImage]);
    }
  }

  function close() {
    $fullscreenPhoto.velocity('fadeOut', {duration: fadeSpeed});
    $homeBtn.velocity('fadeIn', {duration: fadeSpeed});
  }

  $next.on('click', function () {
    next();
  })

  $previous.on('click', function () {
    previous();
  })

  $exit.on('click', function () {
    close();
  })

  $(document).keydown(function(e) {//change active fullscreen photo on keydown
    if ($fullscreenPhoto.hasClass('open')) {
      switch(e.which) {
          case 37: previous();// left
          break;

          case 38: previous();// up
          break;

          case 39: next();// right
          break;

          case 40: next();// down
          break;

          default: return; // exit this handler for other keys
      }
      e.preventDefault(); // prevent the default action (scroll / move caret)
    }
});
  $(document).keyup(function(e) {//exit fullscreen on esc keyup for better compatibility
    if ($fullscreenPhoto.hasClass('open')) {
      switch(e.which) {
          case 27: close();// down
          break;

          default: return; // exit this handler for other keys
      }
      e.preventDefault(); // prevent the default action (scroll / move caret)
    }
});


  $photo.on('click', function() {//close fullscreen photo
    close();
  });
}());
