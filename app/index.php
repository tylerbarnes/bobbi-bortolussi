<?php
$instagram = "'https://instagram.com/bobbibortolussi/'";
$imgPath = "photos";
$dirPaths = glob($imgPath . '/*' , GLOB_ONLYDIR);
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Bobbi Bortolussi">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bobbi Bortolussi - Freelance Photographer</title>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href='http://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,200,600' rel='stylesheet' type='text/css'>
    <!-- Place favicon.ico in the root directory -->

    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- endbuild -->

    <!-- build:js scripts/vendor/modernizr.js -->
    <!-- <script src="/bower_components/modernizr/modernizr.js"></script> -->
    <script src="scripts/modernizr.js"></script>
    <!-- endbuild -->
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id='horizontal-wrapper'>
      <section id='slider'>
        <section id='work'>
          <aside>
            <ul></ul>
          </aside>
          <section id='work-img-container'>
            <?php
            $number = 0;
            foreach ($dirPaths as $path) {
              $images = glob($path . "/*");

              ?>
              <div data-number='<?php echo $number ?>' class='loaded img-section <?php echo substr($path, strpos($path, "/") + 1)?> <?php echo $number ?>'>

                <?php
                  foreach($images as $image) {?>
                        <div class='images' data-style='background:url("<?php echo $image ?>"); background-size:cover'></div>
                <?php } ?>

              </div>

            <?php } ?>

          </section>
          <div id='fullscreen-photo'>
            <div id='photo'>

            </div>
            <nav id='photo-nav'>
              <!-- <div id='previous-button'>previous</div>
              <div id='next-button'>next</div> -->
              <ol>
                <li id='previous-button'>previous</li>
                <li id='exit-button'>exit</li>
                <li id='next-button'>next</li>
              </ol>
            </nav>
          </div>
        </section>

        <section id='about'>
          <div id='about-text'>
            <h2><span>Bobbi </span><br>Bortolussi</h2>
            <h3>Freelance Photographer</h3>
            <p>
              Hello!<br>
              I am a greater Vancouver photographer that takes pride in photographing the true spirit and personality
              of the people and subjects that I photograph. My style has been described as creative, fun, engaging, and relaxed.<br><br>

              I specialize in art photography, weddings, lifestyle, and event photography. I look forward to meeting you and discussing
              your photography needs.

            </p>
          </div>
          <div id='about-bobbi-img'>

          </div>
        </section>

      </section>

      <div id='home-button' class='close-button'>
        <div>
          <div class='vertical-line'></div>
          <div class='vertical-line'></div>
        </div>
      </div>

      <section id='landing' class='in-view'>

        <main>

          <header>
            <h2>BOBBI</h2>
            <div>
            <a href= <?php echo $instagram ?> target='_blank'>INSTAGRAM</a>
            <a id='email-btn'>EMAIL</a>
          </div>
          </header>
          <section>
            <h1>BOBBI <br><span>BORTOLUSSI</span></h1>
            <h2>FREELANCE PHOTOGRAPHY</h2>
          </section>
          <nav>
            <ul id='left-nav'>
              <a data-select='work' id='home-btn' ><li>WORK</li></a>
              <div class='vertical-line'></div>
              <a data-select='about' id='about-btn'><li>ABOUT</li></a>
            </ul>
          </nav>
        </main>

      </section>
    </div>
    <section id='contact'>
        <div id='form-messages'></div>
        <form id='ajax-contact' method='post' action='php/mailer.php'>
          <section>
            <div class='field'>
              <label for='name'>Name:</label>
              <input type='text' id='name' name='name' required>
            </div>
            <div class='field'>
              <label for='email'>Email:</label>
              <input tyoe='email' id='email' name='email' required>
            </div>
            <div class='field'>
              <label for='message'>Message:</label>
              <textarea id='message' name='message' required></textarea>
            </div>
          </section>
          <footer>
            <div class='field'>
              <button type='submit'>Send</button>
            </div>
            <div id='close-mailer' class='close-button'>
              <div>
                <div class='vertical-line'></div>
                <div class='vertical-line'></div>
              </div>
          </footer>
          </div>
        </form>
    </section>



    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!--
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='https://www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
-->

    <!-- build:js scripts/vendor.js -->
    <!-- bower:js -->
    <!-- <script src="/bower_components/jquery/dist/jquery.js"></script> -->
    <!-- <script src="/bower_components/velocity/velocity.min.js"></script> -->
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:js scripts/main.js -->
    <script src="scripts/main-min.js"></script>
    <!-- <script src="scripts/main.js"></script> -->
    <!-- <script src="scripts/photoselect.js"></script> -->
    <!-- endbuild -->
  </body>
</html>
